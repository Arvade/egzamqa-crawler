package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.converter.ElementsToQuestionBlocksConverter;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionCrawlerImpl;
import org.bitbucket.arvade.egzamqa.util.UrlBuilder;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuestionCrawlerImplTest {

    @Mock
    private Map<String, String> headers;

    @Mock
    private Map<String, String> cookies;

    @Mock
    private ElementsToQuestionBlocksConverter elementsToQuestionBlocksConverter;

    @Mock
    private UrlBuilder urlBuilder;

    @InjectMocks
    private QuestionCrawlerImpl service;

    @Mock
    private Element elementWithAnyClass;

    @Mock
    private Element elementWithoutAnyClass;

    @Before
    public void setUp() {
        when(elementWithAnyClass.hasClass(anyString())).thenReturn(true);
        when(elementWithoutAnyClass.hasClass(anyString())).thenReturn(false);
    }

    @Test
    public void shouldRemoveQuestionsThatHaveImage() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Given
        Method removeQuestionsThatHaveImage = service.getClass().getDeclaredMethod("removeQuestionsThatHaveImage", List.class);
        removeQuestionsThatHaveImage.setAccessible(true);

        List<QuestionBlock> questionBlockList = new ArrayList<>();

        List<Element> questionBlockElementListWithoutImage = new ArrayList<>();
        List<Element> questionBlockElementListWithImage = new ArrayList<>();
        questionBlockElementListWithImage.add(elementWithAnyClass);
        questionBlockElementListWithImage.add(elementWithAnyClass);

        questionBlockElementListWithoutImage.add(elementWithoutAnyClass);
        questionBlockElementListWithoutImage.add(elementWithoutAnyClass);

        QuestionBlock questionBlockWithImage = new QuestionBlock(questionBlockElementListWithImage);
        QuestionBlock questionBlockWithoutImage = new QuestionBlock(questionBlockElementListWithoutImage);

        questionBlockList.add(questionBlockWithImage);
        questionBlockList.add(questionBlockWithImage);
        questionBlockList.add(questionBlockWithoutImage);
        questionBlockList.add(questionBlockWithoutImage);
        questionBlockList.add(questionBlockWithoutImage);

        List<QuestionBlock> expected = new ArrayList<>();
        expected.add(questionBlockWithoutImage);
        expected.add(questionBlockWithoutImage);
        expected.add(questionBlockWithoutImage);

        // When
        removeQuestionsThatHaveImage.invoke(service, questionBlockList);

        // Then
        assertEquals(expected, questionBlockList);
    }

    @Test
    public void shouldFilterElementListByClassName() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Given
        String className = "rezultatN";
        Method method = service.getClass().getDeclaredMethod("filterBlockElementsByClassName", List.class, String.class);
        method.setAccessible(true);


        List<Element> elementListToBeFiltered = new ArrayList<>();
        elementListToBeFiltered.add(elementWithAnyClass);
        elementListToBeFiltered.add(elementWithAnyClass);
        elementListToBeFiltered.add(elementWithAnyClass);
        elementListToBeFiltered.add(elementWithoutAnyClass);
        elementListToBeFiltered.add(elementWithoutAnyClass);

        List<Element> expectedElementList = new ArrayList<>();
        expectedElementList.add(elementWithoutAnyClass);
        expectedElementList.add(elementWithoutAnyClass);

        List<QuestionBlock> questionBlockListToBeFiltered = new ArrayList<>();
        questionBlockListToBeFiltered.add(new QuestionBlock(elementListToBeFiltered));

        // When
        method.invoke(service, questionBlockListToBeFiltered, className);

        // Then
        assertEquals(expectedElementList, questionBlockListToBeFiltered.get(0).getElementList());
    }

    @Test
    public void shouldFilterElementListFromElementsWithoutClass() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Given
        Method method = service.getClass().getDeclaredMethod("filterByDivWithoutClass", List.class);
        method.setAccessible(true);

        Element elementWithoutClass = mock(Element.class);
        Element elementWithClass = mock(Element.class);
        doReturn("someClass").when(elementWithClass).className();
        doReturn("").when(elementWithoutClass).className();

        List<Element> elementListToBeFiltered = new ArrayList<>();
        elementListToBeFiltered.add(elementWithClass);
        elementListToBeFiltered.add(elementWithClass);
        elementListToBeFiltered.add(elementWithClass);
        elementListToBeFiltered.add(elementWithoutClass);
        elementListToBeFiltered.add(elementWithoutClass);

        List<Element> expectedElementList = new ArrayList<>();
        expectedElementList.add(elementWithClass);
        expectedElementList.add(elementWithClass);
        expectedElementList.add(elementWithClass);

        List<QuestionBlock> questionBlockListToBeFiltered = new ArrayList<>();
        questionBlockListToBeFiltered.add(new QuestionBlock(elementListToBeFiltered));

        // When
        method.invoke(service, questionBlockListToBeFiltered);

        // Then
        assertEquals(expectedElementList, questionBlockListToBeFiltered.get(0).getElementList());
    }

    @Test
    public void shouldFilterQuestionBlocksByEmptyDivs() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Given
        Method method = service.getClass().getDeclaredMethod("filterByEmptyDivs", List.class);
        method.setAccessible(true);

        List<Element> elementList = new ArrayList<>();
        elementList.add(elementWithAnyClass);

        List<QuestionBlock> emptyQuestionBlocks = new ArrayList<>();

        emptyQuestionBlocks.add(new QuestionBlock(Collections.emptyList()));
        emptyQuestionBlocks.add(new QuestionBlock(Collections.emptyList()));
        emptyQuestionBlocks.add(new QuestionBlock(Collections.emptyList()));
        emptyQuestionBlocks.add(new QuestionBlock(Collections.emptyList()));

        List<QuestionBlock> notEmptyQuestionBlocks = new ArrayList<>();

        notEmptyQuestionBlocks.add(new QuestionBlock(new ArrayList<>(elementList)));
        notEmptyQuestionBlocks.add(new QuestionBlock(new ArrayList<>(elementList)));
        notEmptyQuestionBlocks.add(new QuestionBlock(new ArrayList<>(elementList)));

        List<QuestionBlock> questionBlockListToBeFiltered = new ArrayList<>();
        questionBlockListToBeFiltered.addAll(emptyQuestionBlocks);
        questionBlockListToBeFiltered.addAll(notEmptyQuestionBlocks);


        // When
        method.invoke(service, questionBlockListToBeFiltered);

        // Then
        assertEquals(questionBlockListToBeFiltered, notEmptyQuestionBlocks);
    }
}
