package org.bitbucket.arvade.egzamQA.service;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionServiceImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class QuestionServiceImplTest {

    @Mock
    private QuestionRepository questionRepository;

    @Mock
    private SessionFactory sessionFactory;

    @InjectMocks
    private QuestionServiceImpl questionService;

    @Mock
    private Session sessionMock;

    @Mock
    private Transaction transactionMock;

    private List<Question> expectedOutputQuestionList = new ArrayList<>();

    @Before
    public void setUp() {
        when(questionRepository.findByQuestionContent("null?")).thenReturn(null);
        expectedOutputQuestionList.add(mock(Question.class));
        expectedOutputQuestionList.add(mock(Question.class));
        expectedOutputQuestionList.add(mock(Question.class));

        when(sessionFactory.openSession()).thenReturn(sessionMock);
        when(sessionMock.getTransaction()).thenReturn(transactionMock);
        doNothing().when(transactionMock).commit();


        when(questionRepository.findByQuestionContent("some questions")).thenReturn(expectedOutputQuestionList);
        when(questionRepository.getAmountOfQuestions()).thenReturn(10);
        when(questionRepository.findAll()).thenReturn(expectedOutputQuestionList);
    }

    @Test
    public void shouldReturnListOfQuestions() {
        // Given
        String questionContent = "some questions";

        // When
        List<Question> result = questionService.findByQuestionContent(questionContent);

        // Then
        assertEquals(expectedOutputQuestionList, result);
    }

    @Test
    public void shouldReturnEmptyList() {
        // Given
        String questionContent = "null?";


        // When
        List<Question> result = questionService.findByQuestionContent(questionContent);

        // Then
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void shouldReturnAmountOfQuestions() {
        // Given
        Integer expected = 10;


        // When
        Integer result = questionService.getAmountOfQuestions();

        // Then
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnAllQuestions() {
        //Then
        List<Question> result = questionService.findAll();

        //Then
        assertEquals(expectedOutputQuestionList, result);
    }

    @Test
    public void shouldInvokeSaveMethodOnce() {
        //Given
        Question question = mock(Question.class);

        //When
        questionService.save(question);

        //Then
        verify(questionRepository, times(1)).save(question);
    }

    @Test
    public void shouldOpenTransactionBeforeSave() {
        //Given
        Question question = mock(Question.class);

        //When
        questionService.save(question);

        //Then
        verify(sessionMock, times(1)).beginTransaction();
    }

    @Test
    public void shouldReturnAllRecord() {
        // When
        List<Question> result = questionService.findAll();

        // Then
        assertEquals(expectedOutputQuestionList, result);
    }
}
