package org.bitbucket.arvade.egzamqa.util;

public class StringUtil {


    public String removeNumberingFromQuestionContent(String content) {
        int cutTo = 0;
        char c;
        for (int i = 0; i < 5; i++) {
            c = content.charAt(i);

            if (Character.isDigit(i) || c == '.') {
                cutTo = i;
            }
        }

        return content.substring(cutTo + 1).trim();
    }
}
