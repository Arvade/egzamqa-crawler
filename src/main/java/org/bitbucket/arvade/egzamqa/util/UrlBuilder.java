package org.bitbucket.arvade.egzamqa.util;

import lombok.Setter;
import org.bitbucket.arvade.egzamqa.model.Qualification;

@Setter
public class UrlBuilder {

    private static final String DEFAULT_DOMAIN = "http://egzamin-informatyk.pl/";
    private static final String EGZAM_POSTFIX = "-egzamin-zawodowy-test-online";
    private static final String SUBMIT_EGZAM_POSTFIX = "-odpowiedzi";

    private String defaultDomain = DEFAULT_DOMAIN;
    private String egzamPostfix = EGZAM_POSTFIX;
    private String submitEgzamPostfix = SUBMIT_EGZAM_POSTFIX;
    private Qualification qualification;


    public String getSubmitEgzamUrl() {
        checkIfQualificationIsSet();

        return new StringBuilder()
                .append(defaultDomain)
                .append(qualification.toString().toLowerCase())
                .append(submitEgzamPostfix)
                .toString();
    }

    public String getEgzamUrl() {
        checkIfQualificationIsSet();

        return new StringBuilder()
                .append(defaultDomain)
                .append(qualification.toString().toLowerCase())
                .append(egzamPostfix)
                .toString();
    }

    private void checkIfQualificationIsSet() {
        if (qualification == null) {
            throw new IllegalStateException("you need to set qualification");
        }
    }
}
