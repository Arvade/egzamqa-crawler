package org.bitbucket.arvade.egzamqa.converter;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;

public interface QuestionBlockToQuestionConverter {

    Question convert(QuestionBlock questionBlock);

}
