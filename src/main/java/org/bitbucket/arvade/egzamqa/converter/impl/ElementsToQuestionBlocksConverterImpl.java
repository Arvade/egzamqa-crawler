package org.bitbucket.arvade.egzamqa.converter.impl;

import org.bitbucket.arvade.egzamqa.converter.ElementsToQuestionBlocksConverter;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class ElementsToQuestionBlocksConverterImpl implements ElementsToQuestionBlocksConverter {

    @Override
    public List<QuestionBlock> convert(Elements elements) {
        List<QuestionBlock> questionsAndAnswers = new ArrayList<>();
        int startCutIndex = -1;
        int endCutIndex = -1;

        boolean isStartSet = false;
        boolean isEndSet = false;

        List<Element> elementList = transformElements(elements);

        for (int i = 0; i < elementList.size(); i++) {
            Element element = elementList.get(i);

            if (!isStartSet) {
                startCutIndex = i;
                isStartSet = true;
            }

            if (element.hasClass("sep")) {
                endCutIndex = i;
                isEndSet = true;
            }

            if (isEndSet) {
                List<Element> questionAndAnswers = elementList.subList(startCutIndex, endCutIndex);
                isStartSet = false;
                isEndSet = false;
                questionsAndAnswers.add(new QuestionBlock(questionAndAnswers));
            }
        }

        return questionsAndAnswers;
    }

    private List<Element> transformElements(Elements elements) {
        List<Element> output = new ArrayList<>();

        output.add(elements.get(0));
        output.addAll(elements.get(0).siblingElements());

        return output;
    }
}
