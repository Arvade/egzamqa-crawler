package org.bitbucket.arvade.egzamqa.converter;

import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.jsoup.select.Elements;

import java.util.List;

public interface ElementsToQuestionBlocksConverter {

    List<QuestionBlock> convert(Elements elements);

}
