package org.bitbucket.arvade.egzamqa.converter.impl;

import com.google.inject.Inject;
import lombok.Setter;
import org.bitbucket.arvade.egzamqa.converter.QuestionBlockToQuestionConverter;
import org.bitbucket.arvade.egzamqa.model.Answer;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.bitbucket.arvade.egzamqa.util.StringUtil;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public class QuestionBlockToQuestionConverterImpl implements QuestionBlockToQuestionConverter {

    private static final String CORRECT_ANSWER_CLASS = "odpgood";

    @Setter
    private String correctAnswerClass = CORRECT_ANSWER_CLASS;

    private StringUtil stringUtil;

    @Inject
    public QuestionBlockToQuestionConverterImpl(StringUtil stringUtil) {
        this.stringUtil = stringUtil;
    }

    @Override
    public Question convert(QuestionBlock questionBlock) {
        String questionContent = questionBlock.getElement(0).childNodes().get(0).toString();
        String questionWithoutNumbering = stringUtil.removeNumberingFromQuestionContent(questionContent);

        Element answerElementA = questionBlock.getElement(1);
        Element answerElementB = questionBlock.getElement(2);
        Element answerElementC = questionBlock.getElement(3);
        Element answerElementD = questionBlock.getElement(4);

        Answer answerA = new Answer(answerElementA.childNodes().get(1).toString(), answerElementA.hasClass(correctAnswerClass));
        Answer answerB = new Answer(answerElementB.childNodes().get(1).toString(), answerElementB.hasClass(correctAnswerClass));
        Answer answerC = new Answer(answerElementC.childNodes().get(1).toString(), answerElementC.hasClass(correctAnswerClass));
        Answer answerD = new Answer(answerElementD.childNodes().get(1).toString(), answerElementD.hasClass(correctAnswerClass));

        List<Answer> answerList = new ArrayList<>();
        answerList.add(answerA);
        answerList.add(answerB);
        answerList.add(answerC);
        answerList.add(answerD);

        return new Question(questionWithoutNumbering, answerList);
    }
}
