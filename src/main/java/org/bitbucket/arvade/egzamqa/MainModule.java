package org.bitbucket.arvade.egzamqa;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import org.bitbucket.arvade.egzamqa.converter.ElementsToQuestionBlocksConverter;
import org.bitbucket.arvade.egzamqa.converter.QuestionBlockToQuestionConverter;
import org.bitbucket.arvade.egzamqa.converter.impl.ElementsToQuestionBlocksConverterImpl;
import org.bitbucket.arvade.egzamqa.converter.impl.QuestionBlockToQuestionConverterImpl;
import org.bitbucket.arvade.egzamqa.provider.E14CookiesProvider;
import org.bitbucket.arvade.egzamqa.provider.E14HeadersProvider;
import org.bitbucket.arvade.egzamqa.provider.HibernateConfigurationProvider;
import org.bitbucket.arvade.egzamqa.provider.SessionFactoryProvider;
import org.bitbucket.arvade.egzamqa.repository.AnswerRepository;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.bitbucket.arvade.egzamqa.repository.impl.AnswerRepositoryImpl;
import org.bitbucket.arvade.egzamqa.repository.impl.QuestionRepositoryImpl;
import org.bitbucket.arvade.egzamqa.service.QuestionCrawler;
import org.bitbucket.arvade.egzamqa.service.QuestionService;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionCrawlerImpl;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionServiceImpl;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Map;

public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(QuestionRepository.class).to(QuestionRepositoryImpl.class);
        bind(AnswerRepository.class).to(AnswerRepositoryImpl.class);
        bind(QuestionService.class).to(QuestionServiceImpl.class);
        bind(Configuration.class).toProvider(HibernateConfigurationProvider.class);
        bind(SessionFactory.class).toProvider(SessionFactoryProvider.class);
        bind(QuestionCrawler.class).to(QuestionCrawlerImpl.class);
        bind(ElementsToQuestionBlocksConverter.class).to(ElementsToQuestionBlocksConverterImpl.class);
        bind(QuestionBlockToQuestionConverter.class).to(QuestionBlockToQuestionConverterImpl.class);

        bind(new TypeLiteral<Map<String, String>>() {
        })
                .annotatedWith(Names.named("E14Headers"))
                .toProvider(E14HeadersProvider.class);

        bind(new TypeLiteral<Map<String, String>>() {
        })
                .annotatedWith(Names.named("E14Cookies"))
                .toProvider(E14CookiesProvider.class);
    }
}
