package org.bitbucket.arvade.egzamqa;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.egzamqa.converter.QuestionBlockToQuestionConverter;
import org.bitbucket.arvade.egzamqa.model.Qualification;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.bitbucket.arvade.egzamqa.service.QuestionCrawler;
import org.bitbucket.arvade.egzamqa.service.QuestionService;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionCrawlerImpl;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Main {

    private QuestionCrawler questionCrawler;
    private QuestionService questionService;
    private QuestionBlockToQuestionConverter questionBlockToQuestionConverter;

    public static void main(String[] args) {
        new Main().init();
    }

    public void init() {
        MainModule module = new MainModule();
        Injector injector = Guice.createInjector(module);

        questionCrawler = injector.getInstance(QuestionCrawlerImpl.class);
        questionService = injector.getInstance(QuestionService.class);
        questionBlockToQuestionConverter = injector.getInstance(QuestionBlockToQuestionConverter.class);

        crawl();
    }

    private void crawl() {
        List<QuestionBlock> questionBlocks = questionCrawler.crawlQuestions(Qualification.E14);

        List<Question> questions = questionBlocks.stream()
                .map(questionBlock -> questionBlockToQuestionConverter.convert(questionBlock))
                .collect(Collectors.toList());

        AtomicInteger amountOfNewQuestions = new AtomicInteger();
        questions.forEach(question -> {
            List<Question> questionContentList = questionService.findByQuestionContent(question.getContent());
            if (!questionContentList.isEmpty()) {
                questionService.save(question);
                amountOfNewQuestions.getAndIncrement();
            }
        });
        System.out.println("New questions : " + amountOfNewQuestions.get());
        amountOfNewQuestions.set(0);
    }
}