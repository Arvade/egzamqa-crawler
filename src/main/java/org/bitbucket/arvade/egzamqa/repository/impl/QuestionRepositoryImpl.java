package org.bitbucket.arvade.egzamqa.repository.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class QuestionRepositoryImpl implements QuestionRepository {

    private SessionFactory sessionFactory;

    @Inject
    public QuestionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Question question) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(question);
        session.getTransaction().commit();
        session.close();
    }

    public List<Question> findAll() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Question> resultList = session.createQuery("FROM Question q", Question.class)
                .getResultList();
        session.getTransaction().commit();
        session.close();
        return resultList;
    }

    @Override
    public Optional<Question> findById(Long id) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Question question = session.find(Question.class, id);
        return Optional.ofNullable(question);
    }

    @Override
    public List<Question> findByQuestionContent(String questionContent) {
        Session session = sessionFactory.openSession();

        return session.createQuery("FROM Question q WHERE q.content = :content", Question.class)
                .setParameter("content", questionContent)
                .getResultList();
    }

    @Override
    public Integer getAmountOfQuestions() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        return session.createQuery("SELECT q FROM Question q")
                .getResultList()
                .size();
    }
}
