package org.bitbucket.arvade.egzamqa.repository;

import org.bitbucket.arvade.egzamqa.model.Answer;

public interface AnswerRepository {

    void save(Answer answer);
}
