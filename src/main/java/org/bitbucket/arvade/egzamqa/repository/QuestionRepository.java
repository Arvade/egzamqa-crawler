package org.bitbucket.arvade.egzamqa.repository;


import org.bitbucket.arvade.egzamqa.model.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionRepository {

    List<Question> findAll();

    void save(Question question);

    Integer getAmountOfQuestions();

    Optional<Question> findById(Long id);

    List<Question> findByQuestionContent(String questionContent);
}