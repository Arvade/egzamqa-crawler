package org.bitbucket.arvade.egzamqa.repository.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.egzamqa.model.Answer;
import org.bitbucket.arvade.egzamqa.repository.AnswerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class AnswerRepositoryImpl implements AnswerRepository {

    private SessionFactory sessionFactory;

    @Inject
    public AnswerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Answer answer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(answer);
    }
}
