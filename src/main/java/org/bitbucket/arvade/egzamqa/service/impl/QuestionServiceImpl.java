package org.bitbucket.arvade.egzamqa.service.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.bitbucket.arvade.egzamqa.service.QuestionService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Collections;
import java.util.List;

public class QuestionServiceImpl implements QuestionService {

    private QuestionRepository questionRepository;
    private SessionFactory sessionFactory;

    @Inject
    public QuestionServiceImpl(QuestionRepository questionRepository, SessionFactory sessionFactory) {
        this.questionRepository = questionRepository;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public void save(Question question) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            questionRepository.save(question);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Question> findByQuestionContent(String questionContent) {
        List<Question> byQuestionContent = questionRepository.findByQuestionContent(questionContent);


        if (byQuestionContent == null) {
            return Collections.emptyList();
        } else {
            return byQuestionContent;
        }
    }

    @Override
    public Integer getAmountOfQuestions() {
        return questionRepository.getAmountOfQuestions();
    }
}
