package org.bitbucket.arvade.egzamqa.service.impl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.bitbucket.arvade.egzamqa.converter.ElementsToQuestionBlocksConverter;
import org.bitbucket.arvade.egzamqa.model.Qualification;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;
import org.bitbucket.arvade.egzamqa.service.QuestionCrawler;
import org.bitbucket.arvade.egzamqa.util.UrlBuilder;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class QuestionCrawlerImpl implements QuestionCrawler {

    private List<Qualification> supportedQualifications = Arrays.asList(Qualification.values());

    private Map<String, String> headers;
    private Map<String, String> cookies;
    private ElementsToQuestionBlocksConverter elementsToQuestionBlocksConverter;
    private UrlBuilder urlBuilder;

    @Inject
    public QuestionCrawlerImpl(@Named("E14Headers") Map<String, String> headers,
                               @Named("E14Cookies") Map<String, String> cookies,
                               ElementsToQuestionBlocksConverter elementsToQuestionBlocksConverter,
                               UrlBuilder urlBuilder) {
        this.headers = headers;
        this.cookies = cookies;
        this.elementsToQuestionBlocksConverter = elementsToQuestionBlocksConverter;
        this.urlBuilder = urlBuilder;
    }

    @Override
    public List<QuestionBlock> crawlQuestions(Qualification qualification) {
        if (!supports(qualification)) {
            throw new IllegalArgumentException(qualification.toString() + " is not supported by QuestionClawler[" + supportedQualifications + "]");
        }

        urlBuilder.setQualification(qualification);
        String submitEgzamURL = urlBuilder.getSubmitEgzamUrl();
        String egzamURL = urlBuilder.getEgzamUrl();

        Document documentWithQuestionAndAnswers;
        Connection.Response response;

        try {
            response = getResponseWithSessionId(egzamURL);
            if (!response.hasCookie("PHPSESSID")) {
                throw new RuntimeException("Response with no PHPSESSID cookie");
            }

            cookies.put("PHPSESSID", response.cookie("PHPSESSID"));

            documentWithQuestionAndAnswers = getDocumentWithQuestionsWithAnswers(cookies, submitEgzamURL);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Elements questions = documentWithQuestionAndAnswers.getElementsByClass("trescE");
        List<QuestionBlock> questionBlockList = elementsToQuestionBlocksConverter.convert(questions);

        removeQuestionsThatHaveImage(questionBlockList);
        filterBlockElementsByClassName(questionBlockList, "rezultatN");
        filterByDivWithoutClass(questionBlockList);
        filterByEmptyDivs(questionBlockList);

        return questionBlockList;
    }

    @Override
    public boolean supports(Qualification qualification) {
        return supportedQualifications.contains(qualification);
    }


    private void removeQuestionsThatHaveImage(List<QuestionBlock> questionBlockList) {
        questionBlockList.removeIf(questionBlock -> questionBlock.hasClass("obrazek"));
    }

    private void filterBlockElementsByClassName(List<QuestionBlock> questionBlockList, String className) {
        questionBlockList.forEach(questionBlock -> {
            List<Element> filtered = questionBlock.getElementList().stream()
                    .filter(element -> !element.hasClass(className))
                    .collect(Collectors.toList());
            questionBlock.setElementList(filtered);
        });
    }

    private void filterByDivWithoutClass(List<QuestionBlock> questionBlockList) {
        for (QuestionBlock questionBlock : questionBlockList) {
            List<Element> elementList = questionBlock.getElementList();
            elementList.removeIf(element -> Objects.equals(element.className(), ""));
        }
    }

    private void filterByEmptyDivs(List<QuestionBlock> questionBlockList) {
        questionBlockList.removeIf(questionBlock -> questionBlock.getAmountOfElements() == 0);
    }

    private Document getDocumentWithQuestionsWithAnswers(Map<String, String> cookies, String submitEgzamURL) throws IOException {
        return Jsoup.connect(submitEgzamURL)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
                .method(Connection.Method.GET)
                .headers(headers)
                .cookies(cookies)
                .timeout(7200)
                .get();
    }

    private Connection.Response getResponseWithSessionId(String urlString) {
        try {
            return Jsoup.connect(urlString)
                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
                    .method(Connection.Method.GET)
                    .headers(headers)
                    .timeout(3600)
                    .execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
