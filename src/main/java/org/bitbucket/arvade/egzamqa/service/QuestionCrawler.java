package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.Qualification;
import org.bitbucket.arvade.egzamqa.model.QuestionBlock;

import java.util.List;

public interface QuestionCrawler {

    List<QuestionBlock> crawlQuestions(Qualification qualification);

    boolean supports(Qualification qualification);
}
