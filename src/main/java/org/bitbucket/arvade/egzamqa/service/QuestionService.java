package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.Question;

import java.util.List;

public interface QuestionService {

    void save(Question question);

    List<Question> findByQuestionContent(String questionContent);

    List<Question> findAll();

    Integer getAmountOfQuestions();
}
