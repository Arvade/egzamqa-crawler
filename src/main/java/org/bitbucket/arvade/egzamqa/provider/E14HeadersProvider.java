package org.bitbucket.arvade.egzamqa.provider;

import com.google.inject.Provider;

import java.util.HashMap;
import java.util.Map;

public class E14HeadersProvider implements Provider<Map<String, String>> {

    @Override
    public Map<String, String> get() {
        Map<String, String> headers = new HashMap<>();

        headers.put("Host", "egzamin-informatyk.pl");
        headers.put("Connection", "keep-alive");
        headers.put("Cache-Control", "max-age=0");
        headers.put("Upgrade-Insecure-Requests", "1");
        headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        headers.put("DNT", "1");
        headers.put("Accept-Encoding", "gzip, deflate");
        headers.put("Accept-Language", "pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7");

        return headers;
    }
}
