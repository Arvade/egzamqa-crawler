package org.bitbucket.arvade.egzamqa.provider;

import com.google.inject.Provider;

import java.util.HashMap;
import java.util.Map;

public class E14CookiesProvider implements Provider<Map<String, String>> {

    @Override
    public Map<String, String> get() {
        Map<String, String> cookies = new HashMap<>();

        cookies.put("agreeCookies", "yes");
        cookies.put("_ga", "GA1.2.1716788393.1509951550");
        cookies.put("_gid", "GA1.2.418358848.1514729046");
        cookies.put("__atuvc", "4%7C49%2C0%7C50%2C1%7C51%2C17%7C52%2C14%7C1");
        cookies.put("__atuvs", "5a48ee56e8379f2600d");

        return cookies;
    }
}
