package org.bitbucket.arvade.egzamqa.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class SessionFactoryProvider implements Provider<SessionFactory> {

    private Configuration configuration;

    @Inject
    public SessionFactoryProvider(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SessionFactory get() {
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        return configuration.buildSessionFactory(ssrb.build());
    }
}
