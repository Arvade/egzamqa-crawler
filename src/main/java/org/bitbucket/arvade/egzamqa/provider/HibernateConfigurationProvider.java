package org.bitbucket.arvade.egzamqa.provider;

import com.google.inject.Provider;
import org.bitbucket.arvade.egzamqa.model.Answer;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.hibernate.cfg.Configuration;

public class HibernateConfigurationProvider implements Provider<Configuration> {

    @Override
    public Configuration get() {
        return new Configuration()
                .addAnnotatedClass(Answer.class)
                .addAnnotatedClass(Question.class)
                .setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL57Dialect")
                .setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver")
                .setProperty("hibernate.connection.username", "egzamqa")
                .setProperty("hibernate.connection.password", "passpass")
                .setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/egzamqa")
                .setProperty("hibernate.current_session_context_class", "thread")
                .setProperty("hibernate.hbm2ddl.auto", "update")
                .setProperty("hibernate.connection.pool_size", "100")
                .setProperty("hibernate.useSSL", "false")
                .setProperty("hibernate.connection.CharSet", "utf-8")
                .setProperty("hibernate.connection.characterEncoding", "utf-8")
                .setProperty("hibernate.connection.useUnicode", "true");
    }
}
