package org.bitbucket.arvade.egzamqa.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jsoup.nodes.Element;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class QuestionBlock {

    private List<Element> elementList;

    public boolean hasClass(String className) {
        for (Element element : elementList) {
            if (element.hasClass(className)) {
                return true;
            }
        }

        return false;
    }

    public Element getElement(Integer index) {
        if (index < 0 || index >= elementList.size()) {
            throw new IndexOutOfBoundsException("Question doesn't have of element at index " + index);
        }

        return elementList.get(index);
    }

    public Integer getAmountOfElements() {
        return this.elementList.size();
    }
}
