package org.bitbucket.arvade.egzamqa.model;


public enum Qualification {
    E12("e12"),
    E13("e13"),
    E14("e14");

    private final String text;

    Qualification(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
